const express = require('express');
const router = express.Router();
const { APP_NAME } = process.env;

/* COURSE SERVICE */
router.get('/', function(req, res, next) {
  res.send(`${APP_NAME}`);
});

module.exports = router;
