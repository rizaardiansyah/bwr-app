const getAll = require('./media/getAll');
const getById = require('./media/getById');
const create = require('./media/create');
const destroy = require('./media/destroy');

module.exports = {
  getAll,
  getById,
  create,
  destroy,
};