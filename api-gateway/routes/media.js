const express = require('express');
const router = express.Router();

/* MEDIA SERVICE */
const mediaHandler = require('./handler');

router.get('/', mediaHandler.getAll);
router.get('/:id', mediaHandler.getById);
router.post('/', mediaHandler.create);
router.delete('/:id', mediaHandler.destroy);



module.exports = router;
