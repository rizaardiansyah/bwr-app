const express = require('express');
const router = express.Router();
const { APP_NAME } = process.env;

/* ORDER SERVICE */
router.get('/', function(req, res, next) {
  res.send(`${APP_NAME}`);
});

module.exports = router;
