const express = require('express');
const router = express.Router();

// Dependencies
const isBase64 = require('is-base64');
const base64Img = require('base64-img');
const fs = require('fs');
// Models
const { Media } = require('../models');

// MEDIA SERVICE
// GET MEDIA
router.get('/', async (req, res) => {
  // Get All tb_media
  const media = await Media.findAll({
    attributes: ['id', 'image']
  });

  // Add localhost to data.image
  const mappedMedia = media.map( data => {
    data.image = `${req.get('host')}/${data.image}`;
    return data
  });
  
  // Sucess
  return res.status(200).json({
    status: 'success',
    data: mappedMedia
  });

})

// GET BY ID
router.get('/:id', async (req, res) => {
  const id = req.params.id;
  const media = await Media.findByPk(id, {
    attributes: ['id', 'image']
  });

  // Media Not Found
  if(!media) {
    return res.status(404).json({ status: 'error', message: "media not found"})
  }

  // Sucess
  return res.status(200).json({
    status: 'success',
    data: {
      id: media.id,
      image: `${req.get('host')}/${media.image}`
    }
  });
})

// POST MEDIA
router.post('/', (req, res) => {
  const image = req.body.image;

  // Check Image is Base64
  if(!isBase64(image, { mimeRequired: true })) {
    // Bad Request
    return res.status(400).json({ status: 'error', message: 'Image Not Base64'});
  }

  // Upload Image
  // parameter-> image, path, filename, callback
  base64Img.img(image, './public/images', Date.now(), async (err, filepath) => {
    if(err) {
      return res.status(400).json({ status: 'error', message: err.message });
    }

    // Upload Image to Database
    const filename = filepath.split("\\").pop().split("/").pop();
    const media = await Media.create({ image: `images/${filename}`});
    return res.status(200).json({
      status: 'success',
      data: {
        id: media.id,
        image: `${req.get('host')}/images/${filename}`
      }
    })
    
  })

})

// DELETE MEDIA
router.delete('/:id', async (req,res) => {
  // get id
  const id = req.params.id;
  const media = await Media.findByPk(id);
  
  // Check Data Media
  if(!media) {
    return res.status(404).json({ status: 'error', message: 'media not found' });
  }

  // Delete Media
  fs.unlink(`./public/${media.image}`, async (err) => {
    if(err) {
      return res.status(400).json({ status: 'error', message: err.message });
    }

    await media.destroy();
    return res.status(200).json({
      status: 'success',
      message: 'media was deleted'
    })

  })
})

module.exports = router;
